# Codigo de um Servidor UDP

import socket
import os
import sys
import thread
import time
import subprocess
import multiprocessing
import requests
import json
import cPickle as pickle
from messageClass import *

countries = ["us"]

PORT = 5002
IP = '10.254.225.14'



def look_for_clients( udp_socket, clients ):

	while (1):
		try:
			udp_socket.settimeout(1)
			msg, address = udp_socket.recvfrom(1024)
			print msg
			udp_socket.sendto('Cliente reconhecido!', address)

			log = open("log-server.txt", "a")
			log.write("Cliente reconhecido: "+address[0] +"\n" )			
			log.close()
			if(clients.count(address) == 0):
				clients.append(address)
		except socket.timeout:
					
			pass
	return



log = open("log-server.txt", "w")
log.write("=================================================================\n")
log.write("==========Log de execucao do servidor de streaming udp===========\n")
log.write("========================gms18/smr17==============================\n")
log.close()

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind((IP, PORT))

manager = multiprocessing.Manager() # objeto utilizado para dividir objetos entre processos

clients = manager.list() #define clientes como uma lista de um objeto presente em ambos processos



p1 = multiprocessing.Process(target=look_for_clients, args=(udp_socket, clients))
p1.start()


i = 1;
URL = 'https://newsapi.org/v2/top-headlines?country=' + countries[0] + '&pageSize=1&language=en&apiKey=fde81f15edbf4a1e99dccbf683486bf0'
request = requests.get(URL)
request = request.json()
request = json.dumps(request)
request = json.loads(request)

# for country in countries:
while i < 25:
	news = request["articles"][0]

	text = news['title']+ '\n\n\n' + news['description']
	print text

	msg = Message(i, text)
	log = open("log-server.txt", "a")
	log.write("enviando pacote %d \n" %i)
	log.close()
	for client in clients:
    		udp_socket.sendto(pickle.dumps(msg), client)

	time.sleep(1)
	i = i+1

final= Message(i , 'EOF')
for client in clients:
	log = open("log-server.txt", "a")
	log.write("Fechando conexao com os cliente: "+ client[0] +"\n")
	log.close()
	udp_socket.sendto(pickle.dumps(final), client)

udp_socket.close()
p1.terminate()
print ("transmissao encerrada")
